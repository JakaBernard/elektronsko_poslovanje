<?php

require_once("model/CarDB.php");
require_once("ViewHelper.php");

class CarsController {

    public static function index() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];

        $data = filter_input_array(INPUT_GET, $rules);
        
        
        if (self::checkValues($data)) {
            echo ViewHelper::render("view/car-detail.php", [
                "car" => CarDB::get($data)
            ]);
        } else {
            echo ViewHelper::render("view/car-list.php", [
                "cars" => CarDB::getActive() //vse ali le aktivne?
            ]);
        }
    }
    
    public static function sellerIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $client_cert = filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");
        if ($client_cert == null) {
            die('err: Spremenljivka SSL_CLIENT_CERT ni nastavljena.');
        }
        $cert_data = openssl_x509_parse($client_cert);
        $role = (is_array($cert_data['subject']['OU']) ?
                $cert_data['subject']['OU'][0] : $cert_data['subject']['OU']);
        if($role == "Prodajalci") {
            
        } elseif($role == "Admin") {
            echo ViewHelper::redirect(BASE_URL . "admin");
            return;
        } else {
            echo ViewHelper::redirect(BASE_URL . "odjava");
            return;
        }
        $mail = (is_array($cert_data['subject']['emailAddress']) ?
                        $cert_data['subject']['emailAddress'][0] : $cert_data['subject']['emailAddress']);
        if($mail != $_SESSION["email"]) {
            echo ViewHelper::redirect(BASE_URL . "odjava");
            return;
        }
        
        $data = filter_input_array(INPUT_GET, $rules);

        if (self::checkValues($data)) {
            echo ViewHelper::render("view/car-detail-seller.php", [//todo
                "car" => CarDB::get($data)
            ]);
        } else {
            echo ViewHelper::render("view/car-list-seller.php", [
                "cars" => CarDB::getAll() //vse ali le aktivne?
            ]);
        }
    }
    
    public static function customerIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if (self::checkValues($data)) {
            echo ViewHelper::render("view/car-detail-customer.php", [//todo
                "car" => CarDB::get($data)
            ]);
        } else {
            echo ViewHelper::render("view/car-list-customer.php", [
                "cars" => CarDB::getAll() //vse ali le aktivne?
            ]);
        }
    }
    
    // Nakupovalna kosarica
    public static function cartIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ],
            'do' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    "regexp" => "/^(add_into_cart|update_cart|purge_cart|delete_item)$/"
                ]
            ],
            'kolicina' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        $url = filter_input(INPUT_SERVER, "PHP_SELF", FILTER_SANITIZE_SPECIAL_CHARS);
        $data = filter_input_array(INPUT_POST, $rules);
        switch ($data["do"]) {
            case "add_into_cart":
                try {
                    $car = CarDB::get(array("id" => $data["id"]));
                    if (isset($_SESSION["cart"][$data["id"]])) {
                        $_SESSION["cart"][$data["id"]] ++;
                    } else {
                        $_SESSION["cart"][$data["id"]] = 1;
                    }
                } catch (Exception $exc) {
                    die($exc->getMessage());
                }
                break;
            case "update_cart":
                if (isset($_SESSION["cart"][$data["id"]])) {
                    if ($data["kolicina"] > 0) {
                        $_SESSION["cart"][$data["id"]] = $data["kolicina"];
                    } else {
                        unset($_SESSION["cart"][$data["id"]]);
                    }
                }
                break;
            case "delete_item":
                unset($_SESSION["cart"][$data["id"]]);
                break;
            case "purge_cart":
                unset($_SESSION["cart"]);
                break;
            default:
                break;
        }
        self::customerIndex();
    }
    
    public static function addForm($values = [
        "znamka" => "",
        "model" => "",
        "letnik" => "",
        "cena" => "",
        "st_lastnikov" => ""
    ]) {
        echo ViewHelper::render("view/car-add.php", $values);
    }

    public static function add() {
        $data = filter_input_array(INPUT_POST, self::getRules());

        if (self::checkValues($data)) {
            $id = CarDB::insert($data);
            echo ViewHelper::redirect(BASE_URL . "seller/cars?id=" . $id);
        } else {
            self::addForm($data);
        }
    }

    public static function editForm($car = []) {
        if (empty($car)) {
            $rules = [
                "id" => [
                    'filter' => FILTER_VALIDATE_INT,
                    'options' => ['min_range' => 1]
                ]
            ];

            $data = filter_input_array(INPUT_GET, $rules);

            if (!self::checkValues($data)) {
                throw new InvalidArgumentException();
            }

            $car = CarDB::get($data);
        }

        echo ViewHelper::render("view/car-edit.php", ["car" => $car]);
    }

    public static function edit() {
        $rules = self::getRules();
        $rules["id"] = [
            'filter' => FILTER_VALIDATE_INT,
            'options' => ['min_range' => 1]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            CarDB::update($data);
            ViewHelper::redirect(BASE_URL . "seller/cars?id=" . $data["id"]);
        } else {
            self::editForm($data);
        }
    }

    public static function delete() {
        $rules = [
            'delete_confirmation' => FILTER_REQUIRE_SCALAR,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            CarDB::deactivate($data);
            $url = BASE_URL . "seller/cars";
        } else {
            if (isset($data["id"])) {
                $url = BASE_URL . "seller/cars/edit?id=" . $data["id"];
            } else {
                $url = BASE_URL . "seller/cars";
            }
        }

        ViewHelper::redirect($url);
    }
    
    public static function activate() {
        $rules = [
            'delete_confirmation' => FILTER_REQUIRE_SCALAR,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            CarDB::activate($data);
            $url = BASE_URL . "seller/cars";
        } else {
            if (isset($data["id"])) {
                $url = BASE_URL . "seller/cars/edit?id=" . $data["id"];
            } else {
                $url = BASE_URL . "seller/cars";
            }
        }

        ViewHelper::redirect($url);
    }

    /**
     * Returns TRUE if given $input array contains no FALSE values
     * @param type $input
     * @return type
     */
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }

    /**
     * Returns an array of filtering rules for manipulation cars
     * @return type
     */
    private static function getRules() {
        return [
            'znamka' => FILTER_SANITIZE_SPECIAL_CHARS,
            'model' => FILTER_SANITIZE_SPECIAL_CHARS,
            'cena' => FILTER_VALIDATE_FLOAT,
            'st_lastnikov' => [
                'filter' => FILTER_VALIDATE_INT,
                'min_range' => 0
                ],
            'letnik' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1800,
                    'max_range' => date("Y")
                ]
            ]
        ];
    }

}
