<?php

require_once("model/ProdajalciDB.php");
require_once("model/StrankeDB.php");
require_once("model/NarociloDB.php");
require_once("model/IzdelekNarociloDB.php");
require_once("ViewHelper.php");

class OrdersController {

    // Izris predracuna
    public static function orderIndex() {
        echo ViewHelper::render("view/car-order-customer.php"); 
    }
    
    public static function sellerIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];

        $data = filter_input_array(INPUT_GET, $rules);

        if (self::checkValues($data)) {
            
            $povezave = IzdelekNarociloDB::get(["id_narocilo" => $data["id"]]);
            //var_dump($povezave);
            $carIdxArray = array();
            //var_dump(count($povezave));
            for($i = 0; $i < count($povezave); $i++) {
                //var_dump($povezave[$i]);
                array_push($carIdxArray, $povezave[$i]["id_izdelek"]);
            }
            //var_dump($carIdxArray);
            $cars = CarDB::getMany(["id" => $carIdxArray]);
            foreach($cars as $key => $car){
                foreach($povezave as $povezava) {
                    if($povezava["id_izdelek"] == $car["id"]) {
                        $cars[$key]["kolicina"] = $povezava["kolicina"];
                        break;
                    }
                }
            }
            //var_dump($cars);
            echo ViewHelper::render("view/order-detail.php", [
                "order" => NarociloDB::get($data),
                "cars" => $cars
            ]);
        } else {
            echo ViewHelper::render("view/order-list.php", [
                "orders" => NarociloDB::getAllAvailable()
            ]);
        }
    }
    
    
    // Ob kliku "Potrdi naročilo", ustvarjanje zapisa med naročili, sporočilo o uspešnem naročilu.
    public static function orderComplete() {
        $stranka = isset($_SESSION["email"]) ? $_SESSION["email"] : [];
        $kosara = isset($_SESSION["cart"]) ? $_SESSION["cart"] : [];
        if ($kosara && $stranka):
            $stranka = StrankeDB::getID(array("email" => $stranka));
            $znesek = 0;
            foreach ($kosara as $carid => $kolicina):
                $carDB = CarDB::get(array("id" => $carid));
                $znesek += $carDB["cena"] * $kolicina;
            endforeach;
            $id_narocila = NarociloDB::insert(array("id_stranka" => $stranka["id"], "datum" => date("Y-m-d"), "stanje" => 0, "cena" => $znesek));
            foreach ($kosara as $carid => $kolicina):
                    IzdelekNarociloDB::insert(array("id_izdelek" => $carid, "id_narocilo" => $id_narocila, "kolicina" => $kolicina));
            endforeach;
            unset($_SESSION["cart"]);
            $sporocilo = "Vaše naročilo je bilo uspešno oddano. Številka naročila: ". $id_narocila;
        else:
            $sporocilo = "Nekaj je šlo narobe... :(";
        endif;
        echo ViewHelper::render("view/car-order-customer-end.php", [
                "sporocilo" => $sporocilo
        ]);
    }
    
    // Izpis vseh naročil stranke, oz. podrobnosti enega
    public static function orderList() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $stranka = isset($_SESSION["email"]) ? $_SESSION["email"] : [];
        if ($stranka) {
            $stranka = StrankeDB::getID(array("email" => $stranka));
            $data = filter_input_array(INPUT_GET, $rules);
            if (self::checkValues($data)) {
                echo ViewHelper::render("view/customer-orders-detail.php", [
                    "narocilo" => NarociloDB::get($data)
                ]);
            } else {
                echo ViewHelper::render("view/customer-orders.php", [
                    "narocila" => NarociloDB::getAllCustomer(array("id_stranka" => $stranka["id"]))
                ]);
            }
        } else {
            // napaka
        }
    }
    
    public static function orderSpremeni() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ],
            "stanje" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0, 'max_range' => 3]
            ]
        ];
        //var_dump(INPUT_POST);
        $data = filter_input_array(INPUT_POST, $rules);
        //var_dump($data);
        if(self::checkValues($data)) {
            var_dump($data);
            NarociloDB::updateStanje($data);
            ViewHelper::redirect(BASE_URL . "/seller/orders");
        }
    }
    
    /**
     * Returns TRUE if given $input array contains no FALSE values
     * @param type $input
     * @return type
     */
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
}

