<?php

require_once("model/AdminDB.php");
require_once("model/ProdajalciDB.php");
require_once("model/StrankeDB.php");
require_once("ViewHelper.php");
require_once("model/DB.php");

class UsersController {
    
    // Izris prijavnega obrazca
    public static function loginForm() {
        echo ViewHelper::render("view/login.php");
    }
    
    // Odjava
    public static function logout() {
        session_destroy();
        echo ViewHelper::redirect(BASE_URL . "cars");
    }
    
    // Obravnava zahteve ob prijavi
    public static function login() {
        $rules = [
            "email" => FILTER_SANITIZE_SPECIAL_CHARS,
            "password" => FILTER_SANITIZE_SPECIAL_CHARS,
            "type" => FILTER_SANITIZE_SPECIAL_CHARS
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        $dbh = DBInit::getInstance();
        $tip = $data["type"];
        $query = "SELECT * FROM $tip WHERE email = :email AND geslo = :geslo";
        
        $stmt = $dbh->prepare($query);
        $stmt->bindParam(":email", $data["email"]);
        $pass = sha1($data["password"]."rAnDoMsTrInG420bLaZeIt");
        $stmt->bindParam(":geslo", $pass);
        $stmt->execute();
        $user = $stmt->fetch();

        if ($user) {
            $_SESSION["email"] = $data["email"];
            $_SESSION["tip"] = $data["type"];
            
            if($tip == "prodajalec") {
                echo ViewHelper::redirect(BASE_URL . "seller/cars");
            } else if($tip == "stranka"){
                if($user['status'] == 1) {
                    echo ViewHelper::redirect(BASE_URL . "customer/cars");
                } else {
                    echo "Prijava neuspesna :'(";
                }
            } else if ($tip == "admin") {
                echo ViewHelper::redirect(BASE_URL . "admin");
            } else {
                echo ViewHelper::redirect(BASE_URL . "odjava");
                return;
            }
        } else {
            echo "Prijava neuspesna :'(";
        }
    }
    
    // Izris registracijskega obrazca
    public static function registerForm() {
        echo ViewHelper::render("view/register.php");
    }
    
    // Obravnava zahteve ob registraciji
    public static function register() {
        $googleUrl = "https://www.google.com/recaptcha/api/siteverify";
        $captchaData = [
            "secret" => "6LdYH0AUAAAAADLfI64M2QhO1s8pYe5QQPrmFjEH",
            "response" => $_POST["g-recaptcha-response"]];
        $response = json_decode(self::callApi("POST", $googleUrl, $captchaData));
        //var_dump($response);
        //exit();
        if(!$response->success) {
            echo ViewHelper::render("view/register.php");
            return;
        }
        $data = filter_input_array(INPUT_POST, self::getRules());
        if(self::checkValues($data) && $data["geslo"] == $data["geslo-conf"]) {
            $dbh = DBInit::getInstance();
            $query = "INSERT INTO stranka (status, ime, priimek, email, geslo, naslov, telefon) 
                        VALUES (1, :ime, :priimek, :email, :geslo, :naslov, :telefon)";
            $stmt = $dbh->prepare($query);
            $stmt->bindParam(":ime", $data["ime"]); 
            $stmt->bindParam(":priimek", $data["priimek"]); 
            $stmt->bindParam(":email", $data["email"]);
            $pass = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
            $stmt->bindParam(":geslo", $pass);
            $stmt->bindParam(":naslov", $data["naslov"]); 
            $stmt->bindParam(":telefon", $data["telefon"]);
            $stmt->execute();
            echo ViewHelper::redirect(BASE_URL . "cars");
        }
        echo "Napaka pri registraciji.";
    }
   
    
    //za admina & prodajalce
    //admin:
    public static function admin() {
        $client_cert = filter_input(INPUT_SERVER, "SSL_CLIENT_CERT");
        if ($client_cert == null) {
            die('err: Spremenljivka SSL_CLIENT_CERT ni nastavljena.');
        }
        $cert_data = openssl_x509_parse($client_cert);
        $role = (is_array($cert_data['subject']['OU']) ?
                $cert_data['subject']['OU'][0] : $cert_data['subject']['OU']);
        if($role == "Admin") {
            
        } elseif($role == "Prodajalci") {
            echo ViewHelper::redirect(BASE_URL . "seller/cars");
            return;
        } else {
            echo ViewHelper::redirect(BASE_URL . "odjava");
            return;
        }
        $mail = (is_array($cert_data['subject']['emailAddress']) ?
                        $cert_data['subject']['emailAddress'][0] : $cert_data['subject']['emailAddress']);
        if($mail != $_SESSION["email"]) {
            echo ViewHelper::redirect(BASE_URL . "odjava");
            return;
        }
        echo ViewHelper::render("view/admin.php");
    }
    public static function adminIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        
                
        $data = filter_input_array(INPUT_GET, $rules);
        
        if (self::checkValues($data)) {
            echo ViewHelper::render("view/seller-detail.php", [
                "user" => ProdajalciDB::get($data)
            ]);
        } else {
            echo ViewHelper::render("view/seller-list.php", [
                "users" => ProdajalciDB::getAll()
            ]);
        }
    }
    public static function editAdmin() {
        $rules = self::getRulesAdmin();
        $rules["id"] = [
            'filter' => FILTER_VALIDATE_INT,
            'options' => ['min_range' => 1]
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        $pwd = $data['geslo'];
        $pwd_conf = $data['geslo-conf'];
        unset($data['geslo']);
        unset($data['geslo-conf']);
        if (self::checkValues($data)) {
            $data['geslo'] = $pwd;
            $data['geslo-conf'] = $pwd_conf;
            if ($data['geslo'] === $data['geslo-conf']){
                unset($data['geslo-conf']);
                $data['geslo'] = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
                AdminDB::update($data);
            } else if($data['geslo'] == ""){
                unset($data['geslo-conf']);
                AdminDB::updateNoPwd($data);
            }
            ViewHelper::redirect(BASE_URL . "admin");
        } else {
            self::editAdminForm();
        }
    }
    
    public static function editAdminForm($user = []) {
        if (empty($user)) {
            $rules = [
                "id" => [
                    'filter' => FILTER_VALIDATE_INT,
                    'options' => ['min_range' => 1]
                ]
            ];

            $data = filter_input_array(INPUT_GET, $rules);

            if (!self::checkValues($data)) {
                throw new InvalidArgumentException();
            }

            $user = AdminDB::get($data);
        }
        echo ViewHelper::render("view/admin-edit.php", ["user" => $user]);
    }
    
    //za prodajalce
    public static function addSeller() {
        $data = filter_input_array(INPUT_POST, self::getRulesSeller());

        if (self::checkValues($data) && $data['geslo'] == $data['geslo-conf']) {
            $dbh = DBInit::getInstance();
            $query = "INSERT INTO prodajalec (status, ime, priimek, email, geslo, naslov, telefon) 
                        VALUES (1, :ime, :priimek, :email, :geslo)";
            $stmt = $dbh->prepare($query);
            $stmt->bindParam(":ime", $data["ime"]); 
            $stmt->bindParam(":priimek", $data["priimek"]); 
            $stmt->bindParam(":email", $data["email"]);
            $pass = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
            $stmt->bindParam(":geslo", $pass);
            $stmt->execute();
            echo ViewHelper::redirect(BASE_URL . "admin/sellers");
        } else {
            self::addSellerForm($data);
        }
    }
    
    public static function addSellerForm($values = [
        "ime" => "",
        "priimek" => "",
        "email" => "",
    ]) {
        echo ViewHelper::render("view/seller-add.php", $values);
    }
    
    public static function editSeller() {
        $rules = self::getRulesSeller();
        $rules["id"] = [
            'filter' => FILTER_VALIDATE_INT,
            'options' => ['min_range' => 1]
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        $pwd = $data['geslo'];
        $pwd_conf = $data['geslo-conf'];
        unset($data['geslo']);
        unset($data['geslo-conf']);
        if (self::checkValues($data)) {
            $data['geslo'] = $pwd;
            $data['geslo-conf'] = $pwd_conf;
            if ($data['geslo'] === $data['geslo-conf']){
                unset($data['geslo-conf']);
                $data['geslo'] = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
                ProdajalciDB::update($data);
            } else if($data['geslo'] == ""){
                unset($data['geslo-conf']);
                ProdajalciDB::updateNoPwd($data);
            }
            ViewHelper::redirect(BASE_URL . "admin/sellers?id=" . $data["id"]);
        } else {
            self::editSellerForm($data);
        }
    }
    
    public static function editSellerForm($user = []) {
        if (empty($user)) {
            $rules = [
                "id" => [
                    'filter' => FILTER_VALIDATE_INT,
                    'options' => ['min_range' => 1]
                ]
            ];

            $data = filter_input_array(INPUT_GET, $rules);

            if (!self::checkValues($data)) {
                throw new InvalidArgumentException();
            }

            $user = ProdajalciDB::get($data);
        }

        echo ViewHelper::render("view/seller-edit.php", ["user" => $user]);
    }
    
    public static function deleteSeller() {
        $rules = [
            'delete_confirmation' => FILTER_REQUIRE_SCALAR,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            ProdajalciDB::deactivate($data);
            $url = BASE_URL . "admin/sellers";
        } else {
            if (isset($data["id"])) {
                $url = BASE_URL . "sellers/edit?id=" . $data["id"];
            } else {
                $url = BASE_URL . "admin/sellers";
            }
        }

        ViewHelper::redirect($url);
    }
    
    public static function activateSeller() {
        $rules = [
            'delete_confirmation' => FILTER_REQUIRE_SCALAR,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            ProdajalciDB::activate($data);
            $url = BASE_URL . "admin/sellers";
        } else {
            if (isset($data["id"])) {
                $url = BASE_URL . "admin/sellers/edit?id=" . $data["id"];
            } else {
                $url = BASE_URL . "admin/sellers";
            }
        }

        ViewHelper::redirect($url);
    }
    
    
    
    
    //za stranke
    public static function sellerIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];

        $data = filter_input_array(INPUT_GET, $rules);

        if (self::checkValues($data)) {
            echo ViewHelper::render("view/user-detail.php", [
                "user" => StrankeDB::get($data)
            ]);
        } else {
            echo ViewHelper::render("view/user-list.php", [
                "users" => StrankeDB::getAll()
            ]);
        }
    }
    
    
    public static function addCustomer() {
        $data = filter_input_array(INPUT_POST, self::getRules());

        if (self::checkValues($data) && $data['geslo'] == $data['geslo-conf']) {
            $dbh = DBInit::getInstance();
            $query = "INSERT INTO stranka (status, ime, priimek, email, geslo, naslov, telefon) 
                        VALUES (1, :ime, :priimek, :email, :geslo, :naslov, :telefon)";
            $stmt = $dbh->prepare($query);
            $stmt->bindParam(":ime", $data["ime"]); 
            $stmt->bindParam(":priimek", $data["priimek"]); 
            $stmt->bindParam(":email", $data["email"]);
            $pass = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
            $stmt->bindParam(":geslo", $pass);
            
            $stmt->bindParam(":naslov", $data["naslov"]); 
            $stmt->bindParam(":telefon", $data["telefon"]);
            $stmt->execute();
            echo ViewHelper::redirect(BASE_URL . "seller/users");
        } else {
            self::addCustomerForm($data);
        }
    }
    
    public static function addCustomerForm($values = [
        "ime" => "",
        "priimek" => "",
        "email" => "",
        "telefon" => "",
        "naslov" => ""
    ]) {
        echo ViewHelper::render("view/user-add.php", $values);
    }
    
    public static function editCustomer() {
        $rules = self::getRules();
        $rules["id"] = [
            'filter' => FILTER_VALIDATE_INT,
            'options' => ['min_range' => 1]
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        //var_dump($data);
        //var_dump($data['geslo']);
        //var_dump($data['geslo-conf']);
        //var_dump($data['geslo'] === $data['geslo-conf']);
        //var_dump($data['geslo'] == $data['geslo-conf']);
        //var_dump($data['geslo'] == false);
        $pwd = $data['geslo'];
        $pwd_conf = $data['geslo-conf'];
        unset($data['geslo']);
        unset($data['geslo-conf']);
        //var_dump($data);
        if (self::checkValues($data)) {
            $data['geslo'] = $pwd;
            $data['geslo-conf'] = $pwd_conf;
            if ($data['geslo'] === $data['geslo-conf']){
                
                unset($data['geslo-conf']);
                $data['geslo'] = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
                StrankeDB::update($data);
            } else if($data['geslo'] == ""){
                unset($data['geslo-conf']);
                StrankeDB::updateNoPwd($data);
            }
            ViewHelper::redirect(BASE_URL . "seller/users?id=" . $data["id"]);
        } else {
            self::editCustomerForm($data);
        }
    }
    
    public static function editCustomerForm($user = []) {
        if (empty($user)) {
            $rules = [
                "id" => [
                    'filter' => FILTER_VALIDATE_INT,
                    'options' => ['min_range' => 1]
                ]
            ];

            $data = filter_input_array(INPUT_GET, $rules);

            if (!self::checkValues($data)) {
                throw new InvalidArgumentException();
            }

            $user = StrankeDB::get($data);
        }

        echo ViewHelper::render("view/user-edit.php", ["user" => $user]);
    }
    
    public static function deleteCustomer() {
        $rules = [
            'delete_confirmation' => FILTER_REQUIRE_SCALAR,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            StrankeDB::deactivate($data);
            $url = BASE_URL . "seller/users";
        } else {
            if (isset($data["id"])) {
                $url = BASE_URL . "seller/users/edit?id=" . $data["id"];
            } else {
                $url = BASE_URL . "seller/users";
            }
        }

        ViewHelper::redirect($url);
    }
    
    public static function activateCustomer() {
        $rules = [
            'delete_confirmation' => FILTER_REQUIRE_SCALAR,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_POST, $rules);

        if (self::checkValues($data)) {
            StrankeDB::activate($data);
            $url = BASE_URL . "seller/users";
        } else {
            if (isset($data["id"])) {
                $url = BASE_URL . "seller/users/edit?id=" . $data["id"];
            } else {
                $url = BASE_URL . "seller/users";
            }
        }

        ViewHelper::redirect($url);
    }
    
    public static function profileForm() {
        $email = $_SESSION["email"];
        if(self::jeStranka()) {
            $user = StrankeDB::getID(["email" => $email]);
            echo ViewHelper::render("view/customer-edit-profile.php", ["user" => $user]);
        } elseif(self::jeProdajalec()) {
            $user = ProdajalciDB::getEmail(["email" => $email]);
            echo ViewHelper::render("view/seller-edit-profile.php", ["user" => $user]);
        } 
    }      
    
    
    public static function profileEdit() {
        if(self::jeStranka()) {$rules = self::getRules();}
        else if(self::jeProdajalec()) {$rules = self::getRulesSeller();}
        $rules["id"] = [
            'filter' => FILTER_VALIDATE_INT,
            'options' => ['min_range' => 1]
        ];
        $data = filter_input_array(INPUT_POST, $rules);
        $pwd = $data['geslo'];
        $pwd_conf = $data['geslo-conf'];
        unset($data['geslo']);
        unset($data['geslo-conf']);
        if (self::checkValues($data)) {
            $data['geslo'] = $pwd;
            $data['geslo-conf'] = $pwd_conf;
            if($data["email"] != $_SESSION["email"]) $_SESSION["email"] = $data["email"];
            if ($data['geslo'] === $data['geslo-conf']){
                unset($data['geslo-conf']);
                $data['geslo'] = sha1($data["geslo"]."rAnDoMsTrInG420bLaZeIt");
                if(self::jeStranka()) {StrankeDB::update($data);}
                else if(self::jeProdajalec()) {ProdajalciDB::update($data);}
            } else if($data['geslo'] == ""){
                unset($data['geslo-conf']);
                if(self::jeStranka()) {StrankeDB::updateNoPwd($data);}
                else if(self::jeProdajalec()) {ProdajalciDB::updateNoPwd($data);}
            }
            if(self::jeStranka()) {ViewHelper::redirect(BASE_URL . "customer/cars");}
            else if(self::jeProdajalec()){ViewHelper::redirect(BASE_URL . "seller/cars");}
        } else {ViewHelper::redirect(BASE_URL . "/cars");}
    }
    
    
    /**
     * Returns TRUE if given $input array contains no FALSE values
     * @param type $input
     * @return type
     */
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }

    /**
     * Returns an array of filtering rules for manipulation users
     * @return type
     */
    private static function getRules() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo-conf' => FILTER_SANITIZE_SPECIAL_CHARS,
            'naslov' => FILTER_SANITIZE_SPECIAL_CHARS,
            'telefon' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    private static function getRulesSeller() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo-conf' => FILTER_SANITIZE_SPECIAL_CHARS,
        ];
    }
    
    
    private static function getRulesAdmin() {
        return [
            'ime' => FILTER_SANITIZE_SPECIAL_CHARS,
            'priimek' => FILTER_SANITIZE_SPECIAL_CHARS,
            'email' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo' => FILTER_SANITIZE_SPECIAL_CHARS,
            'geslo-conf' => FILTER_SANITIZE_SPECIAL_CHARS
        ];
    }
    
    private static function encodePassword($pwd) { //ZA NAREST KODIRANJE GESEL
        return $pwd;
    }
    
    
    // Method: POST, PUT, GET etc
    // Data: array("param" => "value") ==> index.php?param=value

    public static function callApi($method, $url, $data = false) {
        $curl = curl_init();
    
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                break;
        }
        
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
    
        curl_close($curl);
        var_dump($result);
        return $result;
    }
    
    public static function jeAdmin() {
        return ($_SESSION["tip"] === "admin");
    }
    
    public static function jeProdajalec() {
        return ($_SESSION["tip"] === "prodajalec");
    }
    
    public static function jeStranka() {
        return ($_SESSION["tip"] === "stranka");
    }
}
