<?php

require_once("model/CarDB.php");
require_once("controller/CarsController.php");
require_once("ViewHelper.php");

class CarsRESTController {

    public static function index() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];

        $data = filter_input_array(INPUT_GET, $rules);
        
        try {
            if (self::checkValues($data)) {
                echo ViewHelper::renderJSON(CarDB::get($data));
            } else {
                echo ViewHelper::renderJSON(CarDB::getActive());
            }
        } catch (InvalidArgumentException $e) {
            echo ViewHelper::renderJSON($e->getMessage(), 404);
        }
        
        
    }
    
    
    
    
    /*
    public static function customerIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ]
        ];
        $data = filter_input_array(INPUT_GET, $rules);
        if (self::checkValues($data)) {
            echo ViewHelper::render("view/car-detail-customer.php", [//todo
                "car" => CarDB::get($data)
            ]);
        } else {
            echo ViewHelper::render("view/car-list-customer.php", [
                "cars" => CarDB::getAll() //vse ali le aktivne?
            ]);
        }
    }
    
    
    // Nakupovalna kosarica
    public static function cartIndex() {
        $rules = [
            "id" => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 1]
            ],
            'do' => [
                'filter' => FILTER_VALIDATE_REGEXP,
                'options' => [
                    "regexp" => "/^(add_into_cart|update_cart|purge_cart|delete_item)$/"
                ]
            ],
            'kolicina' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => ['min_range' => 0]
            ]
        ];
        $url = filter_input(INPUT_SERVER, "PHP_SELF", FILTER_SANITIZE_SPECIAL_CHARS);
        $data = filter_input_array(INPUT_POST, $rules);
        switch ($data["do"]) {
            case "add_into_cart":
                try {
                    $car = CarDB::get(array("id" => $data["id"]));
                    if (isset($_SESSION["cart"][$data["id"]])) {
                        $_SESSION["cart"][$data["id"]] ++;
                    } else {
                        $_SESSION["cart"][$data["id"]] = 1;
                    }
                } catch (Exception $exc) {
                    die($exc->getMessage());
                }
                break;
            case "update_cart":
                if (isset($_SESSION["cart"][$data["id"]])) {
                    if ($data["kolicina"] > 0) {
                        $_SESSION["cart"][$data["id"]] = $data["kolicina"];
                    } else {
                        unset($_SESSION["cart"][$data["id"]]);
                    }
                }
                break;
            case "delete_item":
                unset($_SESSION["cart"][$data["id"]]);
                break;
            case "purge_cart":
                unset($_SESSION["cart"]);
                break;
            default:
                break;
        }
        self::customerIndex();
    }
    */
    
    private static function checkValues($input) {
        if (empty($input)) {
            return FALSE;
        }

        $result = TRUE;
        foreach ($input as $value) {
            $result = $result && $value != false;
        }

        return $result;
    }
    
    

}
