package com.example.tadej.prodajalnavozil;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<List<car>> {

    ArrayList<DataModel> dataModels;
    ListView listView;
    private CustomAdapter adapter;

    @Override
    //Ob kreiranju MainActivitiya
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Log.i("GREM NOTR: ", toString());
        CarService.RestApi api = CarService.getInstance();
        Call<List<car>> call = api.getAll();
        call.enqueue(this);

        //Za listo vozil na glavni strani
        listView=(ListView)findViewById(R.id.listview);
        //sem notr shranim podrobnosti iz JSONa
        dataModels = new ArrayList<>();

        //pove kako mora izgledat ena vrstica in biti oblikovana
        adapter = new CustomAdapter(dataModels, getApplicationContext());
        //nastavi adapter
        listView.setAdapter(adapter);
        //ce kliknes na nek izdelek
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //intent klice nov activity in s tem putExtra da podatke drugmu activitiju
                DataModel dataModel= dataModels.get(position);

                Intent myIntent = new Intent(MainActivity.this, PodrobnostiActivity.class);
                myIntent.putExtra("avto", dataModel.getAvto());
                myIntent.putExtra("model", dataModel.getModel());
                myIntent.putExtra("letnik", dataModel.getLetnik());
                myIntent.putExtra("cena", dataModel.getCena());
                myIntent.putExtra("st_lastnikov", dataModel.getSt_lastnikov());

                MainActivity.this.startActivity(myIntent);
            }
        });
    }

    //po defaultu:
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //dropdown
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.kosaricaMenu) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResponse(Call<List<car>> call, Response<List<car>> response) {
        //Log.i("SYSTEM OUT call:", String.valueOf(call));
        //Log.i("SYSTEM OUT response:", String.valueOf(response));
        for(car avto : response.body()) {
            adapter.add(new DataModel(avto.znamka, avto.model, avto.letnik, avto.cena, avto.st_lastnikov));
            //Log.i("dodajam avto", avto.toString());
        }
    }

    @Override
    public void onFailure(Call<List<car>> call, Throwable t) {
        //Log.i("SYSTEM OUT call fail:", String.valueOf(call));
        Log.e("Throwable", t.toString());
    }
}
