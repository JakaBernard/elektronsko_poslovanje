package com.example.tadej.prodajalnavozil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
    //okno ki se odpre ob kliku na izdelek
    public class PodrobnostiActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_podrobnosti);

            Intent intent = getIntent();
            String avto = intent.getStringExtra("avto");
            String model = intent.getStringExtra("model");
            String letnik = intent.getStringExtra("letnik");
            String cena = intent.getStringExtra("cena");
            String st_lastnikov = intent.getStringExtra("st_lastnikov");

            ((TextView) findViewById(R.id.textView)).setText("Znamka: " + avto);
            ((TextView) findViewById(R.id.textView2)).setText("Model: " + model);
            ((TextView) findViewById(R.id.textView3)).setText("Letnik: " + letnik);
            ((TextView) findViewById(R.id.textView4)).setText("Cena: " + cena + " €");
            ((TextView) findViewById(R.id.textView5)).setText("Število lastnikov: " + st_lastnikov);

            //Kosarica.kosarica.put()

        }
}
