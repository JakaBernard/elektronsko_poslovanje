package com.example.tadej.prodajalnavozil;

import java.io.Serializable;
import java.util.Locale;

public class car implements Serializable {

    public String id;
    public String znamka;
    public String model;
    public String letnik;
    public String cena;
    public String st_lastnikov;


    @Override
    public String toString() {
        return "car{" +
                "id='" + id + '\'' +
                ", znamka='" + znamka + '\'' +
                ", model='" + model + '\'' +
                ", letnik='" + letnik + '\'' +
                ", cena='" + cena + '\'' +
                ", st_lastnikov='" + st_lastnikov + '\'' +
                '}';
    }
}
