package com.example.tadej.prodajalnavozil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener{
    //pove kako se oblikuje en izdelek (ena vrstica)

    Context mContext;

    public CustomAdapter(ArrayList<DataModel> dataModels, Context applicationContext) {
        super(applicationContext, R.layout.list_item, dataModels);
        this.mContext = applicationContext;
    }


    private static class ViewHolder {
        TextView txtFirst;
        TextView txtSecond;
    }

    //po defaultu:
    @Override
    public void onClick(View v) {

    }


    // list_item.xml -> template za eno vrstico
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataModel dataModel = getItem(position);
        ViewHolder viewHolder;


        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            //znamka
            viewHolder.txtFirst = (TextView) convertView.findViewById(R.id.firstLine);
            //model + letnik
            viewHolder.txtSecond = (TextView) convertView.findViewById(R.id.secondLine);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //get metode
        viewHolder.txtFirst.setText(dataModel.getAvto());
        viewHolder.txtSecond.setText(dataModel.getModel() + ", (" + dataModel.getLetnik() + ")");

        //na zaslon:
        return convertView;
    }
}