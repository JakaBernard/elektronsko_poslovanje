package com.example.tadej.prodajalnavozil;


import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class CarService {
    interface RestApi {
        String URL = "http://10.0.2.2:8080/netbeans/elektronsko_poslovanje/index.php/api/";
        @GET("cars")
        Call<List<car>> getAll();
    }

    private static RestApi instance;

    public static synchronized RestApi getInstance() {
        if (instance == null) {
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestApi.URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            instance = retrofit.create(RestApi.class);
        }

        return instance;
    }
}
