package com.example.tadej.prodajalnavozil;


//konstruktor za model
public class DataModel {

    String avto;
    String model;
    String letnik;
    String cena;
    String st_lastnikov;

    public DataModel(String avto, String model, String letnik, String cena, String st_lastnikov) {
        this.avto = avto;
        this.model = model;
        this.letnik = letnik;
        this.cena = cena;
        this.st_lastnikov = st_lastnikov;

    }

    public String getAvto() {
        return avto;
    }

    public String getModel() {
        return model;
    }

    public String getLetnik() {
        return letnik;
    }

    public String getCena() {
        return cena;
    }

    public String getSt_lastnikov() {
        return st_lastnikov;
    }


}
