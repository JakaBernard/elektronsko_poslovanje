<?php
//index.php je ruter

//session za celotno aplikacijo
session_start();

//sem pridej kontrolerji
//oblike: require_once("controller/CarsController.php");
require_once("controller/CarsController.php");
require_once("controller/CarsRESTController.php");
require_once("controller/UsersController.php");
require_once("controller/OrdersController.php");

define("BASE_URL", $_SERVER["SCRIPT_NAME"] . "/");
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "/static/images/"); //ko ali ce bomo imeli slike
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "/static/css/"); //ko ali ce bomo imeli css

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

/*
var_dump(BASE_URL);
var_dump(IMAGES_URL);
var_dump(CSS_URL);
var_dump($path);
exit();
*/



//TODO
$urls = [
    "cars" => function() {
        CarsController::index();
    },
    "seller/cars" => function() {
        if (UsersController::jeProdajalec()) {CarsController::sellerIndex();}
        else {echo "Error #401: Not authorized";}
    },
    "customer/cars" => function() {
        if (UsersController::jeStranka()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                CarsController::cartIndex();  
            } else {
                CarsController::customerIndex();   
            }
        } else {echo "Error #401: Not authorized";}
    },
    "customer/orders" => function() {
        if(UsersController::jeStranka()) {OrdersController::orderList();}
        else {echo "Error #401: Not authorized";}
    },
    "customer/cars/order" => function() {
        if (UsersController::jeStranka()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                OrdersController::orderComplete();
            } else {
                OrdersController::orderIndex();     
            }
        } else {echo "Error #401: Not authorized";}
    },
    "customer/profile" => function() {
        if (UsersController::jeStranka()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::profileEdit();  
            } else {
                UsersController::profileForm();   
            }
        } else {echo "Error #401: Not authorized";}
    },
    "seller/profile" => function() {
        if (UsersController::jeProdajalec()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::profileEdit();  
            } else {
                UsersController::profileForm();   
            }
        } else {echo "Error #401: Not authorized";}
    },      
    "seller/cars/add" => function () {
        if (UsersController::jeProdajalec()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                CarsController::add();
            } else {
                CarsController::addForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
    "seller/cars/edit" => function () {
        if (UsersController::jeProdajalec()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                CarsController::edit();
            } else {
                CarsController::editForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
    "seller/cars/delete" => function () {
        if (UsersController::jeProdajalec()) {CarsController::delete();}
        else {echo "Error #401: Not authorized";}
    },
    "seller/cars/activate" => function () {
        if (UsersController::jeProdajalec()) {CarsController::activate();}
        else {echo "Error #401: Not authorized";}
    },
    "prijava" => function() {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            UsersController::login();
        } else {
            UsersController::loginForm();
        }
    },
    "odjava" => function() {
        UsersController::logout();
    },
    "registracija" => function() {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            UsersController::register();
        } else {
            UsersController::registerForm();
        }
    },
    
    
    //prijava kot admin & urejanje prodajalcev
    //admin:
    "admin" => function() {
        if (UsersController::jeAdmin()) {UsersController::admin();}
        else {echo "Error #401: Not authorized";}
    },
    "admin/sellers" => function() {
        if (UsersController::jeAdmin()) {UsersController::adminIndex();}
        else {echo "Error #401: Not authorized";}
    },
    "admin/edit" => function () {
        if (UsersController::jeAdmin()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::editAdmin();
            } else {
                UsersController::editAdminForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
     
    //za prodajalce:
    "admin/sellers/add" => function () {
        if (UsersController::jeAdmin()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::addSeller();
            } else {
                UsersController::addSellerForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
    "admin/sellers/edit" => function () {
        if (UsersController::jeAdmin()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::editSeller();
            } else {
                UsersController::editSellerForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
    "admin/sellers/delete" => function () {
        if (UsersController::jeAdmin()) {UsersController::deleteSeller();}
        else {echo "Error #401: Not authorized";}
    },
    "admin/sellers/activate" => function () {
        if (UsersController::jeAdmin()) {UsersController::activateSeller();}
        else {echo "Error #401: Not authorized";}
    },
    
    
    
    //za urejanje strank
    
    "seller/users" => function() {
        if (UsersController::jeProdajalec()) {UsersController::sellerIndex();}
        else {echo "Error #401: Not authorized";}
    },
    "seller/users/add" => function () {
        if (UsersController::jeProdajalec()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::addCustomer();
            } else {
                UsersController::addCustomerForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
    "seller/users/edit" => function () {
        if (UsersController::jeProdajalec()) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                UsersController::editCustomer();
            } else {
                UsersController::editCustomerForm();
            }
        } else {echo "Error #401: Not authorized";}
    },
    "seller/users/delete" => function () {
        if (UsersController::jeProdajalec()) {UsersController::deleteCustomer();}
        else {echo "Error #401: Not authorized";}
    },
    "seller/users/activate" => function () {
        if (UsersController::jeProdajalec()) {UsersController::activateCustomer();}
        else {echo "Error #401: Not authorized";}
    },
    
    "seller/orders" => function() {
        if (UsersController::jeProdajalec()) {OrdersController::sellerIndex();}
        else {echo "Error #401: Not authorized";}
    },
    "seller/orders/potrdi" => function() {
        if (UsersController::jeProdajalec()) {OrdersController::orderSpremeni();}
        else {echo "Error #401: Not authorized";}
    },
    "seller/orders/storniraj" => function() {
        if (UsersController::jeProdajalec()) {OrdersController::orderSpremeni();}
        else {echo "Error #401: Not authorized";}
    },
    "seller/orders/preklici" => function() {
        if (UsersController::jeProdajalec()) {OrdersController::orderSpremeni();}
        else {echo "Error #401: Not authorized";}
    },
    
    
    
    
    
    #REST API:
    #za dodat..
    "api/cars" => function() {
        CarsRESTController::index();
    },
    
            
    
    
    
    "" => function () {
        ViewHelper::redirect(BASE_URL . "cars");
    }
    ];



try {
    if (isset($urls[$path])) {
        $urls[$path]();
    } else {
        echo "No controller for '$path'";
    }
} catch (InvalidArgumentException $e) {
    ViewHelper::error404();
} catch (Exception $e) {
    echo "An error occurred: <pre>$e</pre>";
} 
