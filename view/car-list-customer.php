<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov</title>
<style>
    th{background-color: #ffffff;}
    td{text-align: center;}
    tr{background-color: #f2f2f2;}
</style>
<h1>Prodajalna Avtomobilov</h1>

<?php
    echo "Prijavljen si kot: ".$_SESSION["email"]; 
    ?>
    <br>
    [
    <a href="<?= BASE_URL . "customer/orders" ?>">Moja naročila</a> | 
    <a href="<?= BASE_URL . "customer/profile" ?>">Uredi profil</a> |
    <a href="<?= BASE_URL . "odjava" ?>">Odjava</a> 
    ]
<div class="all">
    <div style="width: 40%; height: 50%; float:left;">
        <h3>Pregled vozil</h3>
            <table style="width: 90%">
                <tr>
                    <th>Znamka</th>
                    <th>Model</th> 
                    <th>Letnik</th>
                </tr>
            <?php foreach ($cars as $car): ?>
                <tr>
                    <div class="car">
                        <td><?= $car["znamka"] ?></td>
                        <td><?= $car["model"] ?></td>
                        <td><?= $car["letnik"] ?></td>
                    	<td><a href="<?= BASE_URL . "customer/cars?id=" . $car["id"] ?>">+</a></td>
                        <form action="<?= BASE_URL . "customer/cars"?>"  method="post" style="display: inline-block; margin-left: 2%">
                            <input type="hidden" name="do" value="add_into_cart" />
                            <input type="hidden" name="id" value="<?= $car["id"] ?>" />
                            <td><button type="submit"> V košarico</button></td>
                        </form>
                    </div>
                </tr>
            <?php endforeach; ?>
            </table>
    </div>
    <div style="width: 50%; height: 50%; background-color: rgb(232,232,232); float:right; padding-left: 1%; padding-bottom: 1%">
        <h3>Košarica</h3>
        <ul>
        <?php 
        $kosara = isset($_SESSION["cart"]) ? $_SESSION["cart"] : [];
        if ($kosara):
            $znesek = 0;
            foreach ($kosara as $carid => $kolicina):
                $carDB = CarDB::get(array("id" => $carid));
                $znesek += $carDB["cena"] * $kolicina;
                ?>
                <li>
                    <p><?= $carDB["znamka"]." ".$carDB["model"]?></p>
                    <form style="display: inline-block" action="<?= BASE_URL . "customer/cars"?>" method="post">
                        <input type="hidden" name="do" value="update_cart" />
                        <input type="hidden" name="id" value="<?= $carid ?>" />
                        <label for="kolicina">Količina: </label>
                        <input type="number" name="kolicina" min="0" value="<?= $kolicina ?>" style="width: 30px"/>
                        <button type="submit">Posodobi</button> 
                    </form>
                    <form style="display: inline-block" action="<?= BASE_URL . "customer/cars"?>" method="post">
                        <input type="hidden" name="do" value="delete_item" />
                        <input type="hidden" name="id" value="<?= $carid ?>" />
                        <button type="submit">Odstrani</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
        <p>Skupaj: <b><?= number_format($znesek, 2) ?> EUR</b></p>
        <form action="<?= BASE_URL . "customer/cars"?>" method="post" style="display: inline-block;">
            <input type="hidden" name="do" value="purge_cart" />
            <input type="submit" value="Izprazni košarico" />
        </form>
        <form action="<?= BASE_URL . "customer/cars/order"?>" style="display: inline-block;">
            <input type="submit" value="Na blagajno"/>
        </form>
        <?php else: ?>
            Košarica je prazna :(
        <?php endif; ?>
    </div>
</div>
    

