<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov - Moja naročila</title>
<style>
    th{background-color: #ffffff;}
    td{text-align: center;}
    tr{background-color: #f2f2f2;}
    table{border: 1px solid;}
</style>
<h1>Moja naročila</h1><br>
    [<a href="<?= BASE_URL . "customer/cars" ?>">Pregled vozil</a>]
<div class="all">
    <div style="width: 100%; height: 50%; float:left;">
        <table style="margin-top: 1%">
            <tr>
                <th>Številka</th>
                <th>Datum</th>
                <th>Stanje</th> 
            </tr>
        <?php foreach ($narocila as $narocilo): ?>
            <tr>
                <td><?= $narocilo["id"] ?></td>
                <td><a href="<?= BASE_URL . "customer/orders?id=" . $narocilo["id"] ?>"><?= $narocilo["datum"] ?> </a></td>
                <td>
                    <?php 
                    if($narocilo["stanje"] == 0): echo "Nepotrjeno";
                    elseif($narocilo["stanje"] == 1): echo "Potrjeno";
                    elseif($narocilo["stanje"] == 2): echo "Stornirano";
                    endif;
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
    </div>
</div>
    

