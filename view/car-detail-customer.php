<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Car detail</title>

<h1>Podrobnosti o vozilu: <?= $car["znamka"] ?> <?= $car["model"] ?> (<?= $car["letnik"] ?>)</h1>

<p>[
<a href="<?= BASE_URL . "customer/cars" ?>">Vsa vozila</a>
]</p>

<div class="all">
    <div style="width: 40%; height: 50%; float:left;">
        <ul>
            <li>Znamka: <b><?= $car["znamka"] ?></b></li>
            <li>Model: <b><?= $car["model"] ?></b></li>
            <li>Letnik: <b><?= $car["letnik"] ?></b></li>
            <li>Cena: <b><?= $car["cena"] ?> EUR</b></li>
            <li>Število lastnikov: <i><?= $car["st_lastnikov"] ?></i></li>
        </ul>
        <form action="<?= BASE_URL . "customer/cars?id=".$car["id"]?>"  method="post">
            <input type="hidden" name="do" value="add_into_cart" />
            <input type="hidden" name="id" value="<?= $car["id"] ?>" />
            <button type="submit" style="margin-left: 5%; width: 25%">V košarico</button>
        </form>
    </div>
    <div style="width: 50%; height: 50%; background-color: rgb(232,232,232); float:right; padding-left: 1%; padding-bottom: 1%">
        <h3>Košarica</h3>
        <ul>
        <?php 
        $kosara = isset($_SESSION["cart"]) ? $_SESSION["cart"] : [];
        if ($kosara):
            $znesek = 0;
            foreach ($kosara as $carid => $kolicina):
                $carDB = CarDB::get(array("id" => $carid));
                $znesek += $carDB["cena"] * $kolicina;
                ?>
                <li>
                    <p><?= $carDB["znamka"]." ".$carDB["model"]?></p>
                    <form style="display: inline-block" action="<?= BASE_URL . "customer/cars?id=".$car["id"]?>" method="post">
                        <input type="hidden" name="do" value="update_cart" />
                        <input type="hidden" name="id" value="<?= $carid ?>" />
                        <label for="kolicina">Količina: </label>
                        <input type="number" name="kolicina" min="0" value="<?= $kolicina ?>" style="width: 30px"/>
                        <button type="submit">Posodobi</button> 
                    </form>
                    <form style="display: inline-block" action="<?= BASE_URL . "customer/cars?id=".$car["id"]?>" method="post">
                        <input type="hidden" name="do" value="delete_item" />
                        <input type="hidden" name="id" value="<?= $carid ?>" />
                        <button type="submit">Odstrani</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
        <p>Skupaj: <b><?= number_format($znesek, 2) ?> EUR</b></p>
        <form action="<?= BASE_URL . "customer/cars?id=".$car["id"]?>" method="post" style="display: inline-block;">
            <input type="hidden" name="do" value="purge_cart" />
            <input type="submit" value="Izprazni košarico" />
        </form>
        <form action="<?= BASE_URL . "customer/cars/order"?>" style="display: inline-block;">
            <input type="submit" value="Na blagajno"/>
        </form>
    <?php else: ?>
        Košarica je prazna :(
    <?php endif; ?>
    </div>
</div>