<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Podrobnosti o prodajalcu</title>

<h1>Podrobnosti o prodajalcu: <?= $user["ime"] ?> <?= $user["priimek"] ?></h1>

<p>[
<a href="<?= BASE_URL . "admin/sellers" ?>">Vsi prodajalci</a> |
<a href="<?= BASE_URL . "admin/sellers/add" ?>">Dodaj novega prodajalca</a>
]</p>

<ul>
    <li>Ime: <b><?= $user["ime"] ?></b></li>
    <li>Priimek: <b><?= $user["priimek"] ?></b></li>
    <li>Email: <b><?= $user["email"] ?></b></li>
</ul>

<p>[ <a href="<?= BASE_URL . "admin/sellers/edit?id=" . $user["id"] ?>">Uredi</a> ]
