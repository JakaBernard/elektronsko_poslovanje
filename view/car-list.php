<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov</title>

<h1>Vsa vozila</h1>
<?php
    if(isset($_SESSION["email"])) {
        echo "Prijavljen si kot: ".$_SESSION["email"];
        ?>
        [
        <a href="<?= BASE_URL . "odjava" ?>">Odjava</a>
        ]
        <?php
    } else {
        echo "Nisi prijavljen";
        ?>
        <br>
        [
        <a href="<?= BASE_URL . "registracija" ?>">Registracija</a> /
        <a href="<?= BASE_URL . "prijava" ?>"> Prijava</a>
        ]
        <?php
    }
?>
<br>


<ul>

    <?php foreach ($cars as $car): ?>
       <li><a href="<?= BASE_URL . "cars?id=" . $car["id"] ?>"><?= $car["znamka"] ?>: 
        	<?= $car["model"] ?> (<?= $car["letnik"] ?>)</a></li>
    <?php endforeach; ?>

</ul>
