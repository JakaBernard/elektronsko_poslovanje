<!DOCTYPE html>

<head>
    <title>Prijava</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
     <style>
        body {margin-left: 2%;}
        button {margin-top: 10px;}
        input {margin-top: 5px;}
        h4 {margin-bottom: 10px;}
    </style>
</head>

<body>
    <form action="<?= BASE_URL . "prijava" ?>" method="post" autocomplete="on">
        <h2>Prijava v spletno prodajalno</h2>
        <h4>
            Tip uporabnika:
            <select name="type">
                <option type="radio" value="admin"> Admin</option>
                <option type="radio" value="prodajalec"> Prodajalec</option>
                <option type="radio" value="stranka" selected> Stranka</option>
            </select>
        </h4>
        <input type="text" placeholder="Elektronski naslov" name="email" required><br>
        <input type="password" placeholder="Geslo" name="password" required><br>
        <button type="submit">Prijava</button>
    </form>
</body>