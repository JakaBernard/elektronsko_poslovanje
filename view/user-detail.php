<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Podrobnosti o stranki</title>

<h1>Podrobnosti o stranki: <?= $user["ime"] ?> <?= $user["priimek"] ?></h1>

<p>[
<a href="<?= BASE_URL . "seller/users" ?>">Vse stranke</a> |
<a href="<?= BASE_URL . "seller/users/add" ?>">Dodaj novo stranko</a>
]</p>

<ul>
    <li>Ime: <b><?= $user["ime"] ?></b></li>
    <li>Priimek: <b><?= $user["priimek"] ?></b></li>
    <li>Email: <b><?= $user["email"] ?></b></li>
    <li>Naslov: <b><?= $user["naslov"] ?></b></li>
    <li>Telefon: <i><?= $user["telefon"] ?></i></li>
</ul>

<p>[ <a href="<?= BASE_URL . "seller/users/edit?id=" . $user["id"] ?>">Uredi</a> ]
