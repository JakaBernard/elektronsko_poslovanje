<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov</title>

<h1>Pozdravljeni!</h1>
<?php
    echo "Prijavljen si kot administrator: ".$_SESSION["email"];
    ?>
    [
    <a href="<?= BASE_URL . "odjava" ?>">Odjava</a>
    ]
    <?php
?>
<p>[
<a href="<?= BASE_URL . "admin/edit?id=1" ?>">Uredi svoj profil</a> |
<a href="<?= BASE_URL . "admin/sellers" ?>">Seznam prodajalcev</a>
]</p>
