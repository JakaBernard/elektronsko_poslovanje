<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Podrobnosti o naročilu</title>

<p>[
<a href="<?= BASE_URL . "seller/cars" ?>">Seznam vozil</a> |
<a href="<?= BASE_URL . "seller/users" ?>">Seznam uporabnikov</a> |
<a href="<?= BASE_URL . "seller/orders" ?>">Seznam narocil</a>
]</p>


<h1>Podrobnosti o naročilu: Stranka <?= $order["id_stranka"] ?>: <?= $order["datum"] ?> (<?= $order["cena"] ?> EUR) <?php if( $order["stanje"] === "0") { echo "(nepotrjeno)"; } elseif($order["stanje"] === "1") {echo "(potrjeno)"; } elseif($order["stanje"] === "2") { echo "(preklicano)"; } else { echo "(stornirano)";} ?></h1>

<ul>

    <?php foreach ($cars as $car): ?>
       <li><?= $car["znamka"] ?>: <?= $car["model"] ?> (<?= $car["letnik"] ?>) <?= $car["status"] == 1 ? "" : "(deaktiviran)" ?>, Količina: <?= $car["kolicina"] ?></li>
    <?php endforeach; ?>

</ul>

<form action="<?= BASE_URL . "seller/orders/"?><?php if( $order["stanje"] === "0") { echo "potrdi"; } elseif($order["stanje"] === "1") {echo "storniraj"; } ?>" <?php if($order["stanje"] === "2" || $order["stanje"] === "3") {echo "type='hidden";} ?> method="post">
    <input type="hidden" name="id" value="<?= $order["id"] ?>"  />
    <input type="hidden" name="stanje" value="<?php if( $order["stanje"] === "0") { echo 1; } elseif($order["stanje"] === "1") {echo 2; } ?>"  />
    <label>Ste prepričani, da bi radi <?php if( $order["stanje"] === "0") { echo "potrdili"; } elseif($order["stanje"] === "1") {echo "stornirali"; }?> naročilo?<input type="checkbox" name="delete_confirmation" /></label>
    <button type="submit" class="important"><?php if($order["stanje"] === "0") { echo "Potrdi"; } elseif($order["stanje"] === "1") { echo "Storniraj"; }?></button>
</form>

<form action="<?= BASE_URL . "seller/orders/"?><?php if( $order["stanje"] === "1") { echo "preklici"; }?>" <?php if($order["stanje"] !== "1") {echo "type='hidden";} ?> method="post">
    <input type="hidden" name="id" value="<?= $order["id"] ?>"  />
    <input type="hidden" name="stanje" value="3"  />
    <label>Ste prepričani, da bi radi <?php if( $order["stanje"] === "1") { echo "preklicali"; }?> naročilo?<input type="checkbox" name="delete_confirmation" /></label>
    <button type="submit" class="important"><?php if($order["stanje"] === "1") { echo "Prekliči"; }?></button>
</form>




