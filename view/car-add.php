<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Add entry</title>

<h1>Dodaj novo vozilo</h1>

<p>[
<a href="<?= BASE_URL . "seller/cars" ?>">Vsa vozila</a>

]</p>

<form action="<?= BASE_URL . "cars/add" ?>" method="post">
    <p><label>Znamka: <input type="text" name="znamka" value="<?= $znamka ?>" autofocus /></label></p>
    <p><label>Model: <input type="text" name="model" value="<?= $model ?>" /></label></p>
    <p><label>Letnik: <input type="number" name="letnik" value="<?= $letnik ?>" /></label></p>
    <p><label>Cena: <input type="number" name="cena" value="<?= $cena ?>" /></label></p>
    <p><label>Število lastnikov: <input type="number" name="st_lastnikov" value="<?= $st_lastnikov ?>" /></label></p>
    <p><button>Vstavi</button></p>
</form>
