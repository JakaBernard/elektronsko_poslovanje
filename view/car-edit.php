<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Uredi vnos</title>

<h1>Uredi vnos</h1>

<p>[
<a href="<?= BASE_URL . "seller/cars" ?>">Vsa vozila</a> |
<a href="<?= BASE_URL . "seller/cars/add" ?>">Dodaj novo</a>
]</p>

<form action="<?= BASE_URL . "seller/cars/edit" ?>" method="post">
    <input type="hidden" name="id" value="<?= $car["id"] ?>"  />
    <p><label>Znamka: <input type="text" name="znamka" value="<?= $car["znamka"] ?>" autofocus /></label></p>
    <p><label>Model: <input type="text" name="model" value="<?= $car["model"] ?>" /></label></p>
    <p><label>Letnik: <input type="number" name="letnik" value="<?= $car["letnik"] ?>" /></label></p>
    <p><label>Cena: <input type="number" name="cena" value="<?= $car["cena"] ?>" /></label></p>
    <p><label>Število lastnikov: <input type="number" name="st_lastnikov" value="<?= $car["st_lastnikov"] ?>" /></label></p>
    <p><button>Posodobi</button></p>
</form>
<form action="<?= BASE_URL . "cars/"?><?= $car[status] == 1 ? "delete" : "activate" ?>" method="post">
    <input type="hidden" name="id" value="<?= $car["id"] ?>"  />
    <label>Ste prepričani, da bi radi <?= $car[status] == 1 ? "de" : "" ?>aktivirali vnos? <input type="checkbox" name="delete_confirmation" /></label>
    <button type="submit" class="important"><?= $car[status] == 1 ? "Dea" : "A" ?>ktiviraj</button>
</form>
