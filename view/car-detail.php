<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Car detail</title>

<h1>Podrobnosti o vozilu: <?= $car["znamka"] ?> <?= $car["model"] ?> (<?= $car["letnik"] ?>)</h1>

<p>[
<a href="<?= BASE_URL . "cars" ?>">Vsa vozila</a>
]</p>

<ul>
    <li>Znamka: <b><?= $car["znamka"] ?></b></li>
    <li>Model: <b><?= $car["model"] ?></b></li>
    <li>Letnik: <b><?= $car["letnik"] ?></b></li>
    <li>Cena: <b><?= $car["cena"] ?> EUR</b></li>
    <li>Število lastnikov: <i><?= $car["st_lastnikov"] ?></i></li>
</ul>

