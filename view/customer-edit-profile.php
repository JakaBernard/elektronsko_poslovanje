<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="utf-8" />
<title>Uredi profil</title>
<style>
    body {margin-left: 1%;}
    input {margin-top: 5px;}
    h1 {margin-bottom: 10px;}
</style>
<h1>Uredi profil</h1>
<p>[
<a href="<?= BASE_URL . "customer/cars" ?>">Pregled vozil</a>
]</p>

<form action="<?= BASE_URL . "customer/profile" ?>" method="post">
    <input type="hidden" name="id" value="<?= $user["id"] ?>"  />
    <label>Ime: <input type="text" name="ime" value="<?= $user["ime"] ?>" autofocus /></label><br>
    <label>Priimek: <input type="text" name="priimek" value="<?= $user["priimek"] ?>" /></label><br>
    <label>Email: <input type="email" name="email" value="<?= $user["email"] ?>" /></label><br>
    <label>Geslo: <input type="password" name="geslo" value="" /></label><br>
    <label>Potrdite geslo: <input type="password" name="geslo-conf" value="" /></label><br>
    <label>Naslov: <input type="text" name="naslov" value="<?= $user["naslov"] ?>" /></label><br>
    <label>Telefon: <input type="text" name="telefon" value="<?= $user["telefon"] ?>" /></label><br>
    <input type="submit" value="Posodobi"/>
</form>
