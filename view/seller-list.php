<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov</title>

<h1>Vsi Prodajalci</h1>
<p>[
<a href="<?= BASE_URL . "admin/sellers/add" ?>">Dodaj</a> |
<a href="<?= BASE_URL . "admin" ?>"> Osnovna stran </a>
]</p>

<ul>
    
    <?php foreach ($users as $user): ?>
       <li><a href="<?= BASE_URL . "admin/sellers?id=" . $user["id"] ?>"><?= $user["ime"] ?> 
        	<?= $user["priimek"] ?> <?= $user["status"] == 1 ? "" : "(deaktiviran)" ?></a> </li>
    <?php endforeach; ?>

</ul>