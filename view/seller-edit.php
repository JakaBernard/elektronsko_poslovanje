<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Uredi vnos</title>

<h1>Uredi vnos</h1>

<p>[
<a href="<?= BASE_URL . "admin/sellers" ?>">Vsi prodajalci</a> |
<a href="<?= BASE_URL . "admin/sellers/add" ?>">Dodaj novega prodajalca</a>
]</p>

<form action="<?= BASE_URL . "admin/sellers/edit" ?>" method="post">
    <input type="hidden" name="id" value="<?= $user["id"] ?>"  />
    <p><label>Ime: <input type="text" name="ime" value="<?= $user["ime"] ?>" autofocus /></label></p>
    <p><label>Priimek: <input type="text" name="priimek" value="<?= $user["priimek"] ?>" /></label></p>
    <p><label>Email: <input type="email" name="email" value="<?= $user["email"] ?>" /></label></p>
    <p><label>Geslo: <input type="password" name="geslo" value="" /></label></p>
    <p><label>Potrdite geslo: <input type="password" name="geslo-conf" value="" /></label></p>
    <p><button>Posodobi</button></p>
</form>
<form action="<?= BASE_URL . "admin/sellers/"?><?= $user["status"] == 1 ? "delete" : "activate" ?>" method="post">
    <input type="hidden" name="id" value="<?= $user["id"] ?>"  />
    <label>Ste prepričani, da bi radi <?= $user["status"] == 1 ? "de" : "" ?>aktivirali vnos? <input type="checkbox" name="delete_confirmation" /></label>
    <button type="submit" class="important"><?= $user["status"] == 1 ? "Dea" : "A" ?>ktiviraj</button>
</form>
