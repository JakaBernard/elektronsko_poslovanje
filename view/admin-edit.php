<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Uredi svoj profil</title>

<h1>Uredi svoj profil</h1>

<p>[
<a href="<?= BASE_URL . "admin" ?>">Nazaj</a>
]</p>

<form action="<?= BASE_URL . "admin/edit" ?>" method="post">
    <input type="hidden" name="id" value="<?= $user["id"] ?>"  />
    <p><label>Ime: <input type="text" name="ime" value="<?= $user["ime"] ?>" autofocus /></label></p>
    <p><label>Priimek: <input type="text" name="priimek" value="<?= $user["priimek"] ?>" /></label></p>
    <p><label>Email: <input type="email" name="email" value="<?= $user["email"] ?>" /></label></p>
    <p><label>Geslo: <input type="password" name="geslo" value="" /></label></p>
    <p><label>Potrdite geslo: <input type="password" name="geslo-conf" value="" /></label></p>
    <p><button>Posodobi</button></p>
</form>
