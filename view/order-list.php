<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov</title>

<h1>Vsa naročila</h1>
<?php
    echo "Prijavljen si kot: ".$_SESSION["email"];
    ?>
    [
    <a href="<?= BASE_URL . "odjava" ?>">Odjava</a> | 
    <a href="<?= BASE_URL . "users/edit/password" ?>">Uredi svoje geslo</a>
    ]
    <?php
?>
<p>[
<a href="<?= BASE_URL . "seller/users" ?>">Seznam uporabnikov</a> |
<a href="<?= BASE_URL . "seller/cars" ?>">Seznam avtomobilov</a>
]</p>

<ul>
    
    <?php foreach ($orders as $order): ?>
       <li><a href="<?= BASE_URL . "seller/orders?id=" . $order["id"] ?>">Stranka <?= $order["id_stranka"] ?>: 
                <?= $order["datum"] ?> (<?= $order["cena"] ?> EUR) <?php if( $order["stanje"] === "0") { echo "(nepotrjeno)"; } elseif($order["stanje"] === "1") {echo "(potrjeno)"; } elseif($order["stanje"] === "2") { echo "(preklicano)"; } else { echo "(stornirano)";} ?></a> </li>
    <?php endforeach; ?>

</ul>
