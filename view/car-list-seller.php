<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>"> 
<meta charset="UTF-8" />
<title>Prodajalna Avtomobilov</title>

<h1>Vsa vozila</h1>
<?php
    echo "Prijavljen si kot: ".$_SESSION["email"];
    ?>
    [
    <a href="<?= BASE_URL . "odjava" ?>">Odjava</a>
    ]
    <?php
?>
<p>[
<a href="<?= BASE_URL . "seller/profile" ?>">Uredi profil</a> |   
<a href="<?= BASE_URL . "seller/cars/add" ?>">Dodaj</a> |
<a href="<?= BASE_URL . "seller/users" ?>">Seznam uporabnikov</a> |
<a href="<?= BASE_URL . "seller/orders" ?>">Seznam naročil</a>
]</p>

<ul>

    <?php foreach ($cars as $car): ?>
       <li><a href="<?= BASE_URL . "seller/cars?id=" . $car["id"] ?>"><?= $car["znamka"] ?>: 
        	<?= $car["model"] ?> (<?= $car["letnik"] ?>) <?= $car["status"] == 1 ? "" : "(deaktiviran)" ?></a> </li>
    <?php endforeach; ?>

</ul>
