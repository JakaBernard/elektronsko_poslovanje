<!DOCTYPE html>
<head>
    <title>Prodajalna avtomobilov - podrobnosti naročila</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        th{background-color: #ffffff;}
        td{text-align: center;}
        tr{background-color: #f2f2f2;}
    </style>
</head>
<body>
    <h2>Podrobnosti naročila</h2>
    [<a href="<?= BASE_URL . "customer/cars" ?>">Pregled vozil</a> | 
    <a href="<?= BASE_URL . "customer/orders" ?>">Moja naročila</a>]
    <table style="width: 50%; margin-top: 1%">
        <tr>
            <th>Znamka</th>
            <th>Model</th> 
            <th>Cena artikla</th>
            <th>Količina</th>
        </tr>
        <?php
        $id_narocila = $narocilo["id"];
        $izdelki = IzdelekNarociloDB::get(array("id_narocilo" => $id_narocila));
        foreach ($izdelki as $izdelek): 
            $car = CarDB::get(array("id" => $izdelek["id_izdelek"]));
            ?>
            <tr>
                <td><?=$car["znamka"]?></td>
                <td><?=$car["model"]?></td>
                <td><?=number_format($car["cena"], 2)." EUR"?></td>
                <td><?=$izdelek["kolicina"]?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="2" style="background-color: #ffffff;"></td>
            <td style="background-color: #ffffff; padding-top: 20px; text-align: right"><b>Skupaj: </b></td>
            <td style="background-color: #ffffff; padding-top: 20px; text-align: right"><b><?= number_format($narocilo["cena"], 2) ?> EUR</b></td>
        </tr>
    </table>
</body>
