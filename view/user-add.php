<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Add entry</title>

<h1>Dodaj novo stranko</h1>

<p>[
<a href="<?= BASE_URL . "seller/users" ?>">Vse stranke</a>

]</p>

<form action="<?= BASE_URL . "seller/users/add" ?>" method="post">
    <input type="hidden" name="id" value="<?= $user["id"] ?>"  />
    <p><label>Ime: <input type="text" name="ime" value="<?= $user["ime"] ?>" autofocus /></label></p>
    <p><label>Priimek: <input type="text" name="priimek" value="<?= $user["priimek"] ?>" /></label></p>
    <p><label>Email: <input type="email" name="email" value="<?= $user["email"] ?>" /></label></p>
    <p><label>Naslov: <input type="text" name="naslov" value="<?= $user["naslov"] ?>" /></label></p>
    <p><label>Geslo: <input type="password" name="geslo" value="" /></label></p>
    <p><label>Potrdite geslo: <input type="password" name="geslo-conf" value="" /></label></p>
    <p><label>Telefon: <input type="text" name="telefon" value="<?= $user["telefon"] ?>" /></label></p>
    <p><button>Vstavi</button></p>
</form>
