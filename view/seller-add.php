<!DOCTYPE html>

<link rel="stylesheet" type="text/css" href="<?= CSS_URL . "style.css" ?>">
<meta charset="UTF-8" />
<title>Add entry</title>

<h1>Dodaj novega prodajalca</h1>

<form action="<?= BASE_URL . "seller/users/add" ?>" method="post">
    <p><label>Ime: <input type="text" name="ime" value="" autofocus /></label></p>
    <p><label>Priimek: <input type="text" name="priimek" value="" /></label></p>
    <p><label>Email: <input type="email" name="email" value="" /></label></p>
    <p><label>Geslo: <input type="password" name="geslo" value="" /></label></p>
    <p><label>Potrdite geslo: <input type="password" name="geslo-conf" value="" /></label></p>
    <p><button>Vstavi</button></p>
</form>
