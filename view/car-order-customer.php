<!DOCTYPE html>
<head>
    <title>Prodajalna avtomobilov - predračun</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        th{background-color: #ffffff;}
        td{text-align: center;}
        tr{background-color: #f2f2f2;}
    </style>
</head>
<body>
    <h2>Predračun</h2>
    <table style="width:50%">
        <tr>
            <th>Znamka</th>
            <th>Model</th> 
            <th>Cena artikla</th>
            <th>Količina</th>
        </tr>
        <?php
        $kosara = $_SESSION["cart"];
        $znesek = 0;
        foreach ($kosara as $carid => $kolicina): 
            $car = CarDB::get(array("id" => $carid));
            $znesek += $car["cena"] * $kolicina;
            ?>
            <tr>
                <td><?=$car["znamka"]?></td>
                <td><?=$car["model"]?></td>
                <td><?=number_format($car["cena"], 2)." EUR"?></td>
                <td><?=$kolicina?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="2" style="background-color: #ffffff;"></td>
            <td style="background-color: #ffffff; padding-top: 20px; text-align: right"><b>Skupaj: </b></td>
            <td style="background-color: #ffffff; padding-top: 20px; text-align: right"><b><?= number_format($znesek, 2) ?> EUR</b></td>
        </tr>
    </table>
    <form action="<?= BASE_URL . "customer/cars/order"?>" method="post">
        <a href="<?= BASE_URL . "customer/cars"?>"><input type="button" value="Nazaj" style="margin-left: 38%; margin-top: 3%"/></a>
        <input type="submit" value="Potrdi naročilo" style="margin-left: 1%; margin-top: 3%"/>
    </form>
</body>
