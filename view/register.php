<!DOCTYPE html>

<head>
    <title>Registracija</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
        body {margin-left: 2%;}
        button {margin-top: 10px;}
        input {margin-top: 5px;}
        h4 {margin-bottom: 10px;}
    </style>
</head>
<body>
    <form action="<?= BASE_URL . "registracija" ?>" method="post" autocomplete="on">
        <h2>Registracija v spletno prodajalno</h2>
        <h4>Osnovni podatki: </h4>   
        <input type="text" placeholder="Ime" name="ime" required><br>
        <input type="text" placeholder="Priimek" name="priimek" required><br>
        <input type="text" placeholder="Elektronski naslov" name="email" required><br>
        <input type="password" placeholder="Geslo" name="geslo" required><br>
        <input type="password" placeholder="Potrdite geslo" name="geslo-conf" required><br>
        <input type="text" id="naslov" placeholder="Naslov" name="naslov" required><br>
        <input type="text" id="telefon" placeholder="Telefon" name="telefon" required><br>
        <div class="g-recaptcha" data-sitekey="6LdYH0AUAAAAACE01Nz7ltrIULDl3uQ1ArtlQGKl"></div>
        <button type="submit">Registracija</button>
    </form>
</body>