<?php

require_once 'model/AbstractDB.php';

class NarociloDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO narocilo (id_stranka, id_prodajalec, datum, stanje, cena) "
                        . " VALUES (:id_stranka, NULL, :datum, :stanje, :cena)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE narocilo SET id_stranka = :id_stranka, id_prodajalec = :id_prodajalec, "
                        . "datum = :datum, stanje = :stanje, cena = :cena"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateStanje(array $params) {
        return parent::modify("UPDATE narocilo SET stanje = :stanje"
                        . " WHERE id = :id", $params);
    }
    
    public static function delete(array $id) {
        return parent::modify("DELETE FROM narocilo WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $narocila = parent::query("SELECT id, id_stranka, id_prodajalec, datum, stanje, cena"
                        . " FROM narocilo"
                        . " WHERE id = :id", $id);
        if (count($narocila) == 1) {
            return $narocila[0];
        } else {
            throw new InvalidArgumentException("No such order");
        }
    }
    
    public static function getAllCustomer(array $id) {
        return parent::query("SELECT id, id_stranka, id_prodajalec, datum, stanje, cena"
                        . " FROM narocilo"
                        . " WHERE id_stranka = :id_stranka", $id);
    }

    public static function getAll() {
        return parent::query("SELECT id, id_stranka, id_prodajalec, datum, stanje, cena"
                        . " FROM narocilo"
                        . " ORDER BY id ASC");
    }
    
    public static function getAllAvailable() {
        return parent::query("SELECT id, id_stranka, id_prodajalec, datum, stanje, cena"
                        . " FROM narocilo"
                        . " WHERE stanje IN(0, 1)"
                        . " ORDER BY id ASC");
    }
    
    // Status: 0-oddano narocilo, 1-potrjeno narocilo, 2-stornirano narocilo
    public static function getByStatus(array $status) {
        return parent::query("SELECT id, id_stranka, id_prodajalec, datum, stanje, cena"
                        . " FROM narocilo"
                        . " WHERE stanje = :stanje"
                        . " ORDER BY id ASC", $status);
    }
    
    public static function setNewStatus(array $params) {
        return parent::modify("UPDATE narocilo SET status = :status"
                        . " WHERE id = :id", $params);
    }
}
