<?php

require_once 'model/AbstractDB.php';

class ProdajalciDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO prodajalec (ime, status, priimek, email, geslo) "
                        . " VALUES (:ime, :status, :priimek, :email, :geslo)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE prodajalec SET ime = :ime, priimek = :priimek, "
                        . "email = :email, geslo = :geslo"
                        . " WHERE id = :id", $params);
    }


    public static function updateNoPwd(array $params) {
        return parent::modify("UPDATE prodajalec SET ime = :ime, priimek = :priimek, "
                        . "email = :email"
                        . " WHERE id = :id", $params);
    }
    
    public static function delete(array $id) {
        return parent::modify("DELETE FROM prodajalec WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $prodajalci = parent::query("SELECT id, status, ime, priimek, email, geslo"
                        . " FROM prodajalec"
                        . " WHERE id = :id", $id);
        
        if (count($prodajalci) == 1) {
            return $prodajalci[0];
        } else {
            throw new InvalidArgumentException("No such seller");
        }
    }
    public static function getAll() {
        return parent::query("SELECT id, status,  ime, priimek, email, geslo"
                        . " FROM prodajalec"
                        . " ORDER BY id ASC");
    }
    public static function getEmail(array $email) {
        $prodajalci = parent::query("SELECT id, status, ime, priimek, email"
                        . " FROM prodajalec"
                        . " WHERE email = :email", $email);
        
        if (count($prodajalci) == 1) {
            return $prodajalci[0];
        } else {
            throw new InvalidArgumentException("No such seller");
        }
    }
    
    public static function getActive() {
        return parent::query("SELECT id, status, ime, priimek, email"
                        . " FROM prodajalec"
                        . " WHERE status = 1"
                        . " ORDER BY id ASC");
    }
    
    public static function getData(array $id) {
        $prodajalci = parent::query("SELECT id, status, ime, priimek, email"
                        . " FROM prodajalec"
                        . " WHERE id = :id", $id);
        
        if (count($prodajalci) == 1) {
            return $prodajalci[0];
        } else {
            throw new InvalidArgumentException("No such seller");
        }
    }
    
    public static function deactivate(array $id) {
        return parent::modify("UPDATE prodajalec SET status = 0"
                        . " WHERE id = :id", $id);
    }
    
    public static function activate(array $id) {
        return parent::modify("UPDATE prodajalec SET status = 1"
                        . " WHERE id = :id", $id);
    }

}
