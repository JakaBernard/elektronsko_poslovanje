<?php

require_once 'model/AbstractDB.php';

class CarDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO izdelek (status, znamka, model, letnik, cena, st_lastnikov) "
                        . " VALUES (1, :znamka, :model, :letnik, :cena, :st_lastnikov)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE izdelek SET znamka = :znamka, model = :model, "
                        . "letnik = :letnik, cena = :cena, st_lastnikov = :st_lastnikov"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        return parent::modify("DELETE FROM izdelek WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $cars = parent::query("SELECT id, status, znamka, model, letnik, cena, st_lastnikov"
                        . " FROM izdelek"
                        . " WHERE id = :id", $id);
        
        if (count($cars) == 1) {
            return $cars[0];
        } else {
            throw new InvalidArgumentException("No such car");
        }
    }

    public static function getAll() {
        return parent::query("SELECT id, status, znamka, model, letnik, cena, st_lastnikov"
                        . " FROM izdelek"
                        . " ORDER BY id ASC");
    }
    
    public static function getMany(array $id) {
        //var_dump($id);
        $ids = implode(",", $id["id"]);
        //var_dump($ids);
        return $cars = parent::query("SELECT id, status, znamka, model, letnik, cena, st_lastnikov"
                        . " FROM izdelek"
                        . " WHERE id IN ($ids)");
    }
    
    
    public static function getActive() {
        return parent::query("SELECT id, status, znamka, model, letnik, cena, st_lastnikov"
                        . " FROM izdelek"
                        . " WHERE status = 1"
                        . " ORDER BY id ASC");
    }
    
    public static function deactivate(array $id) {
        return parent::modify("UPDATE izdelek SET status = 0"
                        . " WHERE id = :id", $id);
    }
    
    public static function activate(array $id) {
        return parent::modify("UPDATE izdelek SET status = 1"
                        . " WHERE id = :id", $id);
    }
}
