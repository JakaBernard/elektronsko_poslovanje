<?php

require_once 'model/AbstractDB.php';

class IzdelekNarociloDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO izdelek_narocilo (id_izdelek, id_narocilo, kolicina) "
                        . " VALUES (:id_izdelek, :id_narocilo, :kolicina)", $params);
    }
    
    public static function update(array $params) {
        return parent::modify("UPDATE izdelek_narocilo SET id_izdelek = :id_izdelek, id_narocilo = :id_narocilo, kolicina = :kolicina"
                        . " WHERE id_narocilo = :id_narocilo", $params);
    }
    
    public static function delete(array $params) {
        return parent::modify("DELETE FROM izdelek_narocilo" 
                        ." WHERE id_izdelek = :id_izdelek AND id_narocilo = :id_narocilo", $params);
    }
    
    // Kateri izdeleki so v narocilu z :id_narocila
    public static function get(array $params) {
        return parent::query("SELECT id_izdelek, id_narocilo, kolicina"
                        . " FROM izdelek_narocilo"
                        . " WHERE id_narocilo = :id_narocilo", $params);
    }

    public static function getAll() {
        return parent::query("SELECT id_izdelek, id_narocilo, kolicina"
                        . " FROM izdelek_narocilo"
                        . " ORDER BY id_narocilo ASC");
    }
}
