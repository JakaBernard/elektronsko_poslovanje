<?php

require_once 'model/AbstractDB.php';

class AdminDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO admin (ime, priimek, email, geslo) "
                        . " VALUES (:ime, :priimek, :email, :geslo)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE admin SET ime = :ime, priimek = :priimek, "
                        . "email = :email, geslo = :geslo"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateNoPwd(array $params) {
        return parent::modify("UPDATE admin SET ime = :ime, priimek = :priimek, "
                        . "email = :email"
                        . " WHERE id = :id", $params);
    }
    
    
    public static function delete(array $id) {
        return parent::modify("DELETE FROM admin WHERE id = :id", $id);
    }
    
    public static function getAll() {
        return parent::query("SELECT id, ime, priimek, email, geslo"
                        . " FROM admin"
                        . " ORDER BY id ASC");
    }
    
    public static function get(array $id) {
        $admin = parent::query("SELECT id, ime, priimek, email, geslo"
                        . " FROM admin"
                        . " WHERE id = :id", $id);
        
        if (count($admin) == 1) {
            return $admin[0];
        } else {
            throw new InvalidArgumentException("No such admin");
        }
    }
    
    public static function getData(array $id) {
        $admin = parent::query("SELECT id, ime, priimek, email"
                        . " FROM admin"
                        . " WHERE id = :id", $id);
        
        if (count($admin) == 1) {
            return $admin[0];
        } else {
            throw new InvalidArgumentException("No such admin");
        }
    }
    
   

}
