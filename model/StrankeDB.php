<?php

require_once 'model/AbstractDB.php';

class StrankeDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO stranka (ime, status, priimek, email, geslo, naslov, telefon) "
                        . " VALUES (:ime, :status :priimek, :email, :geslo, :naslov, :telefon)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE stranka SET ime = :ime, priimek = :priimek, "
                        . "email = :email, geslo = :geslo, naslov = :naslov, telefon = :telefon"
                        . " WHERE id = :id", $params);
    }
    
    public static function updateNoPwd(array $params) {
        return parent::modify("UPDATE stranka SET ime = :ime, priimek = :priimek, "
                        . "email = :email, naslov = :naslov, telefon = :telefon"
                        . " WHERE id = :id", $params);
    }

    public static function delete(array $id) {
        return parent::modify("DELETE FROM stranka WHERE id = :id", $id);
    }

    public static function get(array $id) {
        $stranke = parent::query("SELECT id, status, ime, priimek, email, geslo, naslov, telefon"
                        . " FROM stranka"
                        . " WHERE id = :id", $id);
        
        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("No such customer");
        }
    }

    public static function getAll() {
        return parent::query("SELECT id, status,  ime, priimek, email, geslo, naslov, telefon"
                        . " FROM stranka"
                        . " ORDER BY id ASC");
    }
    
    public static function getID(array $email) {
        $stranke = parent::query("SELECT id, status, ime, priimek, email, naslov, telefon"
                        . " FROM stranka"
                        . " WHERE email = :email", $email);
        
        if (count($stranke) == 1) {
            return $stranke[0];
        } else {
            throw new InvalidArgumentException("No such customer");
        }
    }
    
    public static function getActive() {
        return parent::query("SELECT id, status, ime, priimek, email, naslov, telefon"
                        . " FROM stranka"
                        . " WHERE status = 1"
                        . " ORDER BY id ASC");
    }
    
    public static function deactivate(array $id) {
        return parent::modify("UPDATE stranka SET status = 0"
                        . " WHERE id = :id", $id);
    }
    
    public static function activate(array $id) {
        return parent::modify("UPDATE stranka SET status = 1"
                        . " WHERE id = :id", $id);
    }
}
